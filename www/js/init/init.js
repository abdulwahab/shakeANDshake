/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:40:19
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:54:03
 */

(function() {
    "use strict";

    angular.module('gift_app.init', []);

})();
