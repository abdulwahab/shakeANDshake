/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:40:09
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:55:44
 */

(function() {
    "use strict";

    angular.module('gift_app.init', [])

    .constant('$ionicLoadingConfig', {
        template: '<ion-spinner></ion-spinner>'
    })

    .constant('APP', {
        DEFAULT_STATE: '',
        DEFAULT_ROUTE: '/',

        MAIN_STATE: '',
        AUTH_STATE: '',
        SIGNUP_STATE: '',

        API_ADDRESS: '',
        API_ENDPOINT: 'api/v1/'
    });

})();
