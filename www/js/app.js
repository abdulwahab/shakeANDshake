/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 20:05:02
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-14 17:38:03
 */

angular.module('gift_app', ['ionic', 'ngCordova', 'gift_app.services', 'gift_app.init', 'gift_app.directives', 'gift_app.deps', 'gift_app.controllers', 'gift_app.components'])

.constant('ApiEndpoint', {
    url: 'http://139.162.37.73/shake_app/public/api/',
    img: 'http://graph.facebook.com/'
})

.run(function($ionicPlatform, Utils, $state, $http, $cordovaPushV5, $rootScope, User_factory, $localstorage, $cordovaStatusbar) {
    var options = {
        android: {
            senderID: "634263215801"
        },
        ios: {
            alert: "true",
            badge: "true",
            sound: "true"
        },
        windows: {}
    };

    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            $cordovaStatusbar.overlaysWebView(false);
            $cordovaStatusbar.style(1);
            $cordovaStatusbar.styleHex('#ffca47');
        }
        var quitCounter = 1;
        $ionicPlatform.registerBackButtonAction(function() {
            if ($state.current.name == "home" || $state.current.name == "signin") {
                if (quitCounter == 2)
                    navigator.app.exitApp();
                else {
                    Utils.showToast("Press again to exit", "long");
                    quitCounter++;
                }
            } else {
                window.history.back();
            }
        }, 101);


        $state.go('signin');


        if (typeof cordova != 'undefined') {
            // initialize
            $cordovaPushV5.initialize(options).then(function() {
                // start listening for new notifications
                $cordovaPushV5.onNotification();
                // start listening for errors
                $cordovaPushV5.onError();

                // register to get registrationId
                $cordovaPushV5.register().then(function(registrationId) {
                    // save `registrationId` somewhere;
                    console.log("registrationId", registrationId);
                    User_factory.set_user(registrationId);
                })
            });

            // triggered every time notification received
            $rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data) {
                // data.message,
                // data.title,
                // data.count,
                // data.sound,
                // data.image,
                // data.additionalData
                console.log("data.message", data.message);
                console.log("data.title", data.title);
                console.log("data.count", data.count);
                console.log("data.sound", data.sound);
                console.log("data.image", data.image);
                console.log("data.additionalData", data.additionalData.data);
                $localstorage.setObject("notification", data.additionalData.data);
                $state.go("challenge");
            });

            // triggered every time error occurs
            $rootScope.$on('$cordovaPushV5:errorOcurred', function(event, e) {
                // e.message
            });
        }


        var result = $localstorage.getObject('login_userData');
        if (result.data != null) {
            $state.go('home');
        } else {
            $state.go('signin');
        }
        setTimeout(function() {
            if (navigator && navigator.splashscreen) {
                navigator.splashscreen.hide();
            }
        }, 1380);

    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {

    $ionicConfigProvider.scrolling.jsScrolling(false);
    $ionicConfigProvider.views.swipeBackEnabled(false);

    /**
     * Timeout Interceptor for HTTP calls (Set to 10 seconds)
     */

    $httpProvider.interceptors.push(function($rootScope, $q) {
        return {
            request: function(config) {
                config.timeout = 10000;
                config.cache = false;
                return config;
            },
            responseError: function(rejection) {
                return $q.reject(rejection);
            }
        }
    });

});