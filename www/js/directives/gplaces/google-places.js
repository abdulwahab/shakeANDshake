/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:38:54
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:39:01
 */

(function() {
    "use strict";

    angular
        .module('gift_app.directives')
        .directive('googlePlace', googlePlace);


    /**
     * Directive implementation of google plugin for autocomplete
     *
     * @param $http
     * @param $log
     * @param $timeout
     * @param $window
     * @param cordovaPlugins
     * @returns {{require: string, restrict: string, replace: boolean, templateUrl: string, scope: {searchQuery: string, onLocationUpdate: string}, link: link}}
     */
    function googlePlace($http, $log, $timeout, $window, cordovaPlugins) {
        return {
            require: '?ngModel',
            restrict: 'E',
            replace: true,
            templateUrl: 'js/directives/gplaces/googlePlace.html',
            scope: {
                searchQuery: '=ngModel',
                onLocationUpdate: '&'
            },
            link: function(scope, element, attr, CompleteSignUpCtrl) {
                scope.dropDownActive = false;
                scope.locations = [];
                scope.id = attr.id || 'location';
                scope.placeholder = attr.placeholder || 'Location';

                var searchEventTimeout = undefined,
                    inputElement = element.find('input'),
                    service = new google.maps.places.AutocompleteService(),
                    geocoder = new google.maps.Geocoder();

                /**
                 * Triggers when user taps an item from the autocomplete dropdown
                 * @param location
                 */
                scope.selectLocation = function(location) {
                    // reset dropdown and update input with the location chosen
                    scope.dropDownActive = false;
                    scope.searchQuery = location.description;

                    // start geocode for the place id
                    geocoder.geocode({ placeId: location.place_id }, function(results, status) {
                        if (status !== google.maps.GeocoderStatus.OK)
                            return onError('Geocode error [' + status + ']');

                        // get coordinates and declare a timestamp UNIX format
                        var xLat = results[0].geometry.location.lat(),
                            xLng = results[0].geometry.location.lng(),
                            xStamp = Date.now() / 1000;

                        var url = "https://maps.googleapis.com/maps/api/timezone/json?sensor=false&timestamp=" + xStamp + "&location=" + xLat + ',' + xLng;
                        $.ajax({
                            url: url,
                        }).done(function(res) {
                            if (res.status !== 'OK')
                                return onError('Timezone error [' + res.status + ']');

                            // update the view from the controller
                            if (scope.onLocationUpdate)
                                scope.onLocationUpdate()(xLat, xLng, res.timeZoneId, res.timeZoneName);
                        });

                        // get the timezone based on coordinates
                        //var url = 'https://maps.googleapis.com/maps/api/timezone/json?sensor=false&timestamp='+ xStamp +'&location='+ xLat +','+ xLng;
                        //scope.onLocationUpdate()(xLat, xLng, "", "");
                        //            $http({
                        //              method: 'GET',
                        //              url: url,
                        //              headers : {
                        //                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                        //              }
                        //            }).then(function(res) {
                        //              if (res.data.status !== 'OK')
                        //                return onError('Timezone error ['+ res.data.status +']');
                        //
                        //              // update the view from the controller
                        //              if (scope.onLocationUpdate)
                        //                scope.onLocationUpdate()(xLat, xLng, res.data.timeZoneId, res.data.timeZoneName);
                        //            });
                    });
                };

                /**
                 * Watcher for the search input - looks for changes
                 */
                scope.$watch('searchQuery', function(query) {
                    // set dropdown status based on the input query and locations already returned
                    if (!query)
                        query = '';
                    scope.dropDownActive = (query.length >= 3 && scope.locations.length);

                    // clear the queue
                    if (searchEventTimeout)
                        $timeout.cancel(searchEventTimeout);

                    // add in queue
                    searchEventTimeout = $timeout(function() {
                        // ignore if input empty or not focused
                        if (!query || inputElement[0] !== $window.document.activeElement)
                            return;
                        // ignore when input is less than 3 characters
                        if (query.length < 3) {
                            scope.locations = [];
                            return;
                        }

                        // get places for the autocomplete dropdown
                        var req = { types: ['geocode'] };
                        req.input = query;
                        service.getPlacePredictions(req, function(results, status) {
                            if (status !== google.maps.places.PlacesServiceStatus.OK)
                                return $log.log(status);

                            // update dropdown and locations array with the results
                            scope.$apply(function() {
                                scope.locations = results;
                                scope.dropDownActive = true;
                            });
                        });
                    }, 350);
                });

                /**
                 * Handles api errors
                 *
                 * @param error
                 */
                var onError = function(error) {
                    $log.error(error);

                    cordovaPlugins.doToast({ message: error });
                    cordovaPlugins.doVibrate();
                }

                /**
                 * Updates dropdown status when focused
                 *
                 * @returns {*}
                 */
                var onClick = function() {
                    return $timeout(function() {
                        scope.dropDownActive = scope.locations.length;
                        scope.$digest();
                    }, 0);
                };

                /**
                 * Updates dropdown status when blurred
                 *
                 * @returns {*}
                 */
                var onCancel = function() {
                    return $timeout(function() {
                        scope.dropDownActive = false;
                        scope.$digest();
                    }, 200);
                };

                inputElement.bind('click', onClick);
                inputElement.bind('touchend', onClick);
                inputElement.bind('blur', onCancel);
            }
        }
    }

})();
