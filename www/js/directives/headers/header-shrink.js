/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:38:00
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:38:25
 */

(function() {
    "use strict";

    angular
        .module('gift_app.directives')
        .directive('headerShrink', headerShrink);


    /**
     * Directive implementation for header-shrink object data
     *
     * @param _
     * @param $document
     * @returns {{restrict: string, link: link}}
     */
    function headerShrink(_, $document, $ionicScrollDelegate) {
        return {
            restrict: 'A',
            link: function($scope, $element) {
                var header = $document[0].body.querySelector('.bar-subheader'),
                    subHeader = null,
                    tabs = null,

                    headerHeight = header.offsetHeight,
                    defHeaderHeight = header.offsetHeight,
                    y = 40,
                    prevY = 40;


                /**
                 * Fetch elements when navigation changed
                 */
                $scope.$parent.$on('$ionicView.enter', function() {
                    _.defer(function() {
                        subHeader = subHeader ? subHeader : $document[0].body.querySelector('.module-view.pane[nav-view="active"] > .bar-subheader');
                        tabs = tabs ? tabs : $document[0].body.querySelector('.module-view.pane[nav-view="active"] .tabs-top');

                        //headerHeight = defHeaderHeight;
                    });
                });

                /**
                 * Restore content offset upon entering a new view
                 */
                $scope.$parent.$on('$ionicView.beforeEnter', function() {
                    $ionicScrollDelegate.scrollTop();
                });

                /**
                 * Restore bar status upon leaving a view
                 */
                $scope.$parent.$on('$ionicView.beforeLeave', function() {
                    doReset(header);
                    doReset(subHeader);
                    doReset(tabs);
                });


                /**
                 * Animate subheader, if tabs present, first then header
                 *
                 * @param offset
                 */
                function translateElements(offset) {
                    if (!_.isNull(tabs))
                        doShrink(tabs, offset, headerHeight, false);

                    if (!_.isNull(subHeader))
                        doShrink(subHeader, offset, headerHeight, !_.isNull(tabs));

                    if (_.isNull(tabs)) {
                        doShrink(header, offset, headerHeight, true);
                    } else if (offset >= defHeaderHeight)
                        doShrink(header, offset - defHeaderHeight, headerHeight, true);

                    if (offset === 0)
                        doReset(header);
                }

                /**
                 * On scroll calculate the offset that needs applied to our elements
                 */
                $element
                    .bind('scroll', function(e) {

                        // calculate the size to update header with
                        var offsetTop = $ionicScrollDelegate.getScrollPosition().top || e.target.scrollTop || window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0,
                            maxOffset = (!_.isNull(tabs)) ? headerHeight / 0.5 : headerHeight;

                        // move Y downwards to show when scrolling up
                        if (offsetTop >= (headerHeight / 0.5)) {
                            y = Math.min(maxOffset, Math.max(0, y + offsetTop - prevY));
                        }
                        // move Y upwards to hide when scrolling down
                        else {
                            y = 0;
                        }

                        translateElements(y);
                        prevY = offsetTop;
                    });
            }
        }

        //////////

        /**
         * Reset an element with its children
         *
         * @param element
         */
        function doReset(element) {
            ionic.requestAnimationFrame(function() {
                if (_.isElement(element)) {
                    element.style[ionic.CSS.TRANSFORM] = '';

                    _.each(element.children, function(el) {
                        el.style.opacity = 1;
                    });
                }
            });
        }

        /**
         * Animate an element with its children
         *
         * @param element
         * @param size
         * @param max
         * @param fade
         */
        function doShrink(element, size, max, fade) {
            var fadeValue = 1 - (size / max);


            ionic.requestAnimationFrame(function() {
                //element.style[ionic.CSS.TRANSITION_DURATION] = '250ms';
                element.style[ionic.CSS.TRANSFORM] = 'translate3d(0, ' + -size + 'px, 0)';

                if (!fade) {
                    return;
                }

                _.each(element.children, function(el) {
                    el.style.opacity = fadeValue;
                });
            });
        }

    }

})();
