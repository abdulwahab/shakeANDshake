/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:38:30
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:38:42
 */


(function() {
    "use strict";

    angular
        .module('gift_app.directives')
        .directive('barColorSwitcher', barColorSwitcher);


    /**
     * Directive implementation for bar-color-switcher object data
     *
     * @param _
     * @param $document
     * @returns {{restrict: string, scope: {color: string}, compile: compile}}
     */
    function barColorSwitcher(_, $document) {
        return {
            restrict: 'A',
            scope: {
                color: '@barColorSwitcher'
            },
            compile: function(element) {
                var ionicNavBar = $document[0].body.querySelector('ion-nav-bar.bar-header'),
                    activeHeaderBar = ionicNavBar.querySelector('.nav-bar-block[nav-bar="active"] .bar-header'),
                    cachedHeaderBar = ionicNavBar.querySelector('.nav-bar-block[nav-bar="cached"] .bar-header'),

                    colors = ['positive', 'balanced', 'energized'],
                    previousColors = [],

                    /**
                     * Adds a new color to navbars
                     *
                     * @param color
                     */
                    addColor = function(color) {
                        ionicNavBar.classList.add('bar-' + color);
                        activeHeaderBar.classList.add('bar-' + color);
                        cachedHeaderBar.classList.add('bar-' + color);
                    },

                    /**
                     * Removes a color from navbars
                     *
                     * @param color
                     */
                    remColor = function(color) {
                        ionicNavBar.classList.remove('bar-' + color);
                        activeHeaderBar.classList.remove('bar-' + color);
                        cachedHeaderBar.classList.remove('bar-' + color);
                    },

                    /**
                     * Resets the navbars, storing the previously used colors (default)
                     */
                    cleanUp = function() {
                        _.each(colors, function(color) {
                            var currentColor = ionicNavBar.classList.contains('bar-' + color);

                            if (currentColor)
                                previousColors.push(color);

                            remColor(color);
                        });
                    },

                    /**
                     * Marks the view and updates the navbars
                     *
                     * @param color
                     */
                    switchColor = function(color) {
                        element[0].setAttribute('data-colorbar', 'true');

                        cleanUp();
                        addColor(color);
                    },

                    /**
                     * Callback triggered on view events (before, enter)
                     *
                     * @param color
                     */
                    eventView = function(color) {
                        // check if the view is marked as updated from before
                        if (element[0].dataset && element[0].dataset.colorbar)
                            return;

                        switchColor(color);
                    },

                    /**
                     * Callback trigered on state event (change)
                     *
                     * @param color
                     */
                    eventStart = function(color) {
                        // reset the view flag
                        element[0].removeAttribute('data-colorbar');

                        // restore previous colors
                        remColor(color);
                        _.each(previousColors, function(color) {
                            addColor(color);
                        });

                        // empties the color store
                        previousColors = [];
                    };

                return function($scope) {
                    $scope.$parent.$on('$ionicView.beforeEnter', function() {
                        eventView.call(this, $scope.color);
                    });

                    $scope.$parent.$on('$ionicView.enter', function() {
                        eventView.call(this, $scope.color);
                    });

                    $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
                        // all states includes the main abstract which is in [app.*] format
                        var fromParent = fromState.name.split('.').splice(0, 2).join('.'),
                            toParent = toState.name.split('.').splice(0, 2).join('.');

                        // ignore if coming from the same parent as all have the same color bar
                        if (fromParent === toParent)
                            return;

                        eventStart.call(this, $scope.color);
                    });
                };
            }
        }
    }

})();
