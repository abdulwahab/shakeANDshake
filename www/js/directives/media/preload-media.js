/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:37:14
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:37:26
 */

(function() {
    "use strict";

    angular
        .module('gift_app.directives')
        .directive('preloadMedia', preloadingMediaBlock)
        .directive('onLoadedMedia', mediaOnLoaded);


    /**
     * Directive implementation for preload-media object
     *
     * @returns {{restrict: string, transclude: boolean, scope: {helper: string}, controller: controller, templateUrl: string}}
     */
    function preloadingMediaBlock() {
        return {
            restrict: 'E',
            transclude: true,
            scope: {
                helper: '@',
                caption: '@'
            },
            controller: function($scope, $timeout) {
                $scope.completed = false;

                /**
                 * Triggers
                 */
                this.startLoading = function() {
                    $scope.$apply(function() {
                        $scope.completed = false;
                    });
                }

                /**
                 * Triggers
                 */
                this.endLoading = function() {
                    $timeout(function() {
                        $scope.completed = true;
                    });
                }
            },
            templateUrl: 'js/directives/media/preloadMedia.html'
        }
    }

    /**
     * Directive implementation for on-loaded-media object data
     *
     * @returns {{restrict: string, require: string, scope: {ngSrc: string}, link: link}}
     */
    function mediaOnLoaded() {
        return {
            restrict: 'A',
            require: '^preloadMedia',
            scope: {
                ngSrc: '@'
            },
            link: function($scope, element, attrs, preloadMediaController) {
                element
                    .on('load', function() {
                        preloadMediaController.endLoading();
                    })
                    .on('error', function() {
                        preloadMediaController.startLoading();
                    });
            }
        }
    }

})();
