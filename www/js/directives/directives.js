/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:33:04
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-14 15:56:16
 */

(function() {
    "use strict";

    angular.module('gift_app.directives', []);
    
})();
