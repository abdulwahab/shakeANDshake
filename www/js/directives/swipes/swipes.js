/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 18:40:51
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:36:51
 */


(function() {
    "use strict";

    angular
        .module('gift_app.directives')
        .directive('swipeNav', swipeNav)
        .directive('swipeNavOff', swipeNavOff);


    /**
     * Directive implementation for swipe left
     *
     * @param $state
     * @param $ionicGesture
     * @returns null
     */
    function swipeNav($ionicGesture, $location, $state, $window, $ionicSideMenuDelegate) {
        return {
            restrict: 'EAC',
            link: function(scope, elem, attr) {
                var curState = attr.state;
                /**
                 * You must add states in stateTransitions array with following params
                 * state [mandatory , String]
                 * right [Optional , boolean]
                 * left [Optional , boolean]
                 * menu [Optional , boolean]
                 */
                var stateTransitions = [];

                var goNext = _.find(stateTransitions, function(item) {
                    return item.state == curState;
                });


                $ionicGesture.on('swiperight', function(event) {
                    event.preventDefault();
                    if (goNext.left)
                        $state.transitionTo(goNext.left);

                }, elem);

                $ionicGesture.on('swipeleft', function(event) {
                    event.preventDefault();
                    if (goNext.right)
                        $state.transitionTo(goNext.right);
                    else if (goNext.menu)
                        $ionicSideMenuDelegate.toggleRight();
                }, elem);

            }
        }
    }

    /**
     * Directive implementation for swipe off
     *
     * @param $state
     * @param $ionicGesture
     * @returns null
     */
    function swipeNavOff($ionicGesture) {
        return {
            restrict: 'EAC',
            link: function(scope, elem, attr) {

                $ionicGesture.on('swiperight', function(event) {
                    console.log('jjhh')
                    event.stopPropagation();
                }, elem);

                $ionicGesture.on('swipeleft', function(event) {
                    event.stopPropagation();
                }, elem);

            }
        }
    }

})();
