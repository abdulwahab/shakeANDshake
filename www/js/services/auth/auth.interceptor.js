/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:53:29
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:55:48
 */

(function() {
    "use strict";

    angular
        .module('gift_app.services')
        .factory('AuthInterceptor', AuthInterceptor)
        .config(function($httpProvider) {
            // updates the http config to use the authInterceptor for all requests
            $httpProvider.interceptors.push('AuthInterceptor');
        });


    /**
     * Factory implementation of the authorization http interceptor
     *
     * @param $q
     * @param $location
     * @param CredentialsService
     * @param GO
     * @returns {{request: request, responseError: responseError}}
     * @constructor
     */
    function AuthInterceptor(_, $q, $location, CredentialsService, GO, $rootScope) {
        return {
            request: request,
            responseError: responseError
        }

        //////////

        /**
         * On requests update the headers with the signed user credentials
         *
         * @param config
         * @returns {*}
         */
        function request(config) {
            var token = CredentialsService.getToken();

            // url which should be sent request without bearer token
            if (config.url.lastIndexOf('https://www.googleapis.com', 0) === 0) {
                return config;
            }

            // sign all requests with the token stored if found any
            if (!!token) {
                config.headers = config.headers || {};
                config.headers.Authorization = 'Bearer ' + token;
            }
            return config;
        }

        /**
         * On errors handle them based on http and inner statuses
         *
         * @param error
         * @returns {*}
         */
        function responseError(error) {
            var errorMessage = null;

            // TODO: add errors to constants & support locale

            // error found
            if (error) {
                // console.log(error)
                // go through all http statuses
                switch (error.status) {

                    case 401:

                        break;

                    default:
                        errorMessage = 'Server error encountered: [' + error.data.status + ']. If the problem persists, contact us.'
                }
            }

            // returns promise with the error
            return $q.reject(_.extend(error, { message: errorMessage }));
        }
    }

})();