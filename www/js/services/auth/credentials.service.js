/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:56:19
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:56:30
 */

(function() {
    "use strict";

    angular
        .module('gift_app.services')
        .factory('CredentialsService', CredentialsFactory);


    /**
     * Factory implementation of the credentials service - store user details locally
     *
     * @param localStorageService
     * @returns {{getToken: getToken, getUser: getUser, setToken: setToken, setUser: setUser, clear: clear}}
     * @constructor
     */
    function CredentialsFactory(localStorageService) {
        // cached data
        var _user = null,
            _token = null;

        return {
            getToken: getToken,
            getUser: getUser,

            setToken: setToken,
            setUser: setUser,

            clear: clear
        }

        //////////

        /**
         * Returns the user token if found any
         *
         * @returns {*}
         */
        function getToken() {
            if (!_token)
                _token = localStorageService.get('token');

            return _token;
        }

        /**
         * Returns the user object if found any
         *
         * @returns {*}
         */
        function getUser() {
            if (!_user)
                _user = localStorageService.get('user');

            return _user;
        }

        /**
         * Saves the user token locally
         *
         * @param token
         */
        function setToken(token) {
            _token = token;
            localStorageService.set('token', _token);
        }

        /**
         * Saves the user object locally
         *
         * @param user
         */
        function setUser(user) {
            _user = user;
            localStorageService.set('user', _user);
        }

        /**
         * Removes the credentials from localstorage
         */
        function clear() {
            _token = null;
            _user = null;

            localStorageService.remove('token', 'user');
        }

    }

})();
