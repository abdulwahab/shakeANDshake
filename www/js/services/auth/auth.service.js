/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:55:58
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:56:07
 */


(function() {
    "use strict";

    angular
        .module('gift_app.services')
        .provider('AuthService', AuthProvider);


    /**
     * Provider implementation of the authorization service - uses api
     *
     * @constructor
     */
    function AuthProvider() {
        this.$get = function($log, $rootScope, $httpParamSerializer, Restangular, paramRestangular, CredentialsService) {

            return {
                signIn: signIn,
                signOut: signOut,
                signUp: signUp,
                completeSignUp: completeSignUp,
                getCurrentUser: getCurrentUser,
                isAuthenticated: isAuthenticated,
                forgot_password: forgot_password,
                facebook_signIn: facebook_signIn,
                facebook_signUp: facebook_signUp
            };

            //////////

            /**
             * Signs user in and store the credentials for future requests
             *
             * @param user
             * @returns {*}
             */
            function signIn(user) {
                return paramRestangular
                    .all('/auth/signIn')
                    .post($httpParamSerializer(user))
                    .then(function(res) {
                        CredentialsService.setToken(res.data.access_token);
                        CredentialsService.setUser(res.data.user);
                        localStorage.setItem('user', JSON.stringify(user));
                        $rootScope.$broadcast('user:updated', res.data.user);

                    });
            }

            /**
             * Forgot Password
             *
             * @param user
             * @returns {*}
             */
            function forgot_password(param) {
                return paramRestangular
                    .all('/password')
                    .customDELETE('', param)
                    .then(function(res) {
                        console.log(res);
                    });
            }

            /**
             * Signs user via Facebook and store the credentials for future requests
             *
             * @param user
             * @returns {*}
             */
            function facebook_signIn(params) {
                return paramRestangular
                    .all('/social/signIn/facebook')
                    .post($httpParamSerializer(params))
                    .then(function(res) {
                        CredentialsService.setToken(res.data.access_token);
                        CredentialsService.setUser(res.data.user);
                        $rootScope.$broadcast('user:updated', res.data.user);

                    });
            }

            /**
             * Signs user via Facebook and store the credentials for future requests
             *
             * @param user
             * @returns {*}
             */
            function facebook_signUp(params) {
                return paramRestangular
                    .all('/social/signUp/facebook')
                    .post($httpParamSerializer(params))
                    .then(function(res) {
                        CredentialsService.setToken(res.data.access_token);
                        CredentialsService.setUser(res.data.user);
                        $rootScope.$broadcast('user:updated', res.data.user);

                    });
            }

            /**
             * Signs user out and removes the credentials stored
             *
             * @returns {*}
             */
            function signOut() {
                return paramRestangular
                    .one('/auth/signOut')
                    .post()
                    .then(function() {
                        CredentialsService.clear();
                    });
            }

            /**
             * Signs user up
             *
             * @param email
             * @returns {*}
             */
            function signUp(email) {
                return paramRestangular
                    .all('/auth/signUp')
                    .post($httpParamSerializer(email));
            }

            /**
             * Completes the signup process and signs in the user
             *
             * @param user
             * @returns {*}
             */
            function completeSignUp(user) {
                return paramRestangular
                    .all('/auth/completeSignUp')
                    .post($httpParamSerializer(user))
                    .then(function(res) {
                        //console.log(res);
                        CredentialsService.setToken(res.data.access_token);
                        CredentialsService.setUser(res.data.user);
                        $rootScope.$broadcast('user:updated', res.data.user);
                    });
            }

            /**
             * Finds the current signed user
             *
             * @returns {*}
             */
            function getCurrentUser() {
                return CredentialsService.getUser();
            }

            /**
             * Finds if user is signed in
             *
             * @returns {boolean}
             */
            function isAuthenticated() {
                return !!CredentialsService.getToken();
            }

        };
    }

})();
