/*
 * @Author: bilalakmal
 * @Date:   2017-02-14 16:08:50
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-14 16:13:19
 */

(function() {
    "use strict";

    angular.module('gift_app.services', [])

    .factory('MyHttpRequest', ['$http', '$q', "$httpParamSerializerJQLike", 'ApiEndpoint', '$rootScope', function($http, $q, $httpParamSerializerJQLike, ApiEndpoint, $rootScope) {
        return function(path, method, data, dataToStringify, maxRetriesFlag, noBaseUrl) {
            var MAX_REQUESTS = 3,
                counter = 1,
                results = $q.defer(),
                headers = {
                    'Content-Type': undefined
                };
            //try {
            //  if (navigator.network.connection.type == 'none') {
            //    results.resolve('internet_issue');
            //    return results.promise;
            //  }
            //} catch (error) {}
            var _token = localStorage.getItem('_token');
            if (dataToStringify)
                data = JSON.stringify(data);
            else {
                // data = $httpParamSerializerJQLike(data);
                headers = {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': _token ? _token : ''
                };
            }


            var request = function() {
                $http({
                        method: method,
                        url: ApiEndpoint.url + path,
                        withCredentials: false,
                        headers: headers,
                        params: data,
                    })
                    .success(function(response) {
                        if (response.Errorcode == 1003) {
                            signOut();
                        }
                        results.resolve(response)
                    })
                    .error(function(error) {
                        if (error.error.code == 401) {
                            localStorage.removeItem('login_userData');
                            localStorage.removeItem('_token');
                            localStorage.removeItem('notification');
                            $rootScope.$broadcast('session_expired');
                        } else {
                            if (counter < MAX_REQUESTS && maxRetriesFlag) {
                                request();
                                counter++;
                            } else {
                                results.resolve(null);
                                return results.promise;
                            }
                        }

                    });
            };

            request();
            return results.promise;
        }
    }])

    .factory('Utils', ['$window', '$q', '$cordovaNetwork', function($window, $q, $cordovaNetwork) {
        return {
            showToast: function(msg, duration) {
                var isIOS = ionic.Platform.isIOS();
                var isAndroid = ionic.Platform.isAndroid()
                if (isAndroid || isIOS) {
                    window.plugins.toast.showWithOptions({
                        message: msg,
                        duration: duration,
                        position: "center"
                            // addPixelsY: -40
                    });
                } else {
                    alert(msg);
                }
            },
            checkConnection: function() {
                if (ionic.Platform.device().uuid) {
                    if ($cordovaNetwork.isOnline())
                        return true;
                    else
                        return false;
                } else
                    return true;
            },
            log: function(message) {
                console.log(message);
            },
            validateEmail: function(email) {
                var ereg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                return ereg.test(email);
            },
            isPhone: function() {
                var isIOS = ionic.Platform.isIOS();
                var isAndroid = ionic.Platform.isAndroid()
                if (isAndroid || isIOS)
                    return true;
                else
                    return false;
            },
            hideKeyBoard: function() {
                var isIOS = ionic.Platform.isIOS();
                var isAndroid = ionic.Platform.isAndroid()
                if (isAndroid || isIOS) {
                    cordova.plugins.Keyboard.close();
                }
            },

            openSite: function(url) {
                if (!$cordovaInAppBrowser)
                    return;
                var options = {
                    location: 'yes',
                    clearcache: 'no',
                    toolbar: 'no'
                };

                $cordovaInAppBrowser.open(url, '_system', options)
                    .then(function(event) {
                        // success
                    })
                    .catch(function(event) {
                        // error
                    });
            }
        }
    }])

    .factory('User_factory', function() {
        var credentials = null;
        var employeeData = null;
        return {
            set_user: function(data) {
                credentials = data;
                console.log(data);
            },
            get: function() {
                return credentials;
            },
            get_session: function() {
                return credentials.sessionKey;
            },
            get_user: function() {
                return credentials;
            },
            // get_user: function() {
            //     return credentials.user_id;
            // },
            set_emp: function(data) {
                employeeData = data;
                console.log(data);
            },
            get_emp: function() {
                console.log('employeeData services', employeeData);
                return employeeData;

            }
        }
    })

    .factory('Employees', function() {
        var employee = null;
        return {
            set_employee: function(emp) {
                employee = emp;
            },
            get_employee: function() {
                return employee;
            }
        }
    })

    .factory('$localstorage', ['$window', function($window) {
        return {
            set: function(key, value) {
                $window.localStorage[key] = value;
            },
            get: function(key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function(key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function(key) {
                return JSON.parse($window.localStorage[key] || '{}');
            },
            remove: function(key) {
                $window.localStorage.removeItem(key);
            },
            getlanguageObject: function(key) {
                return JSON.parse($window.localStorage[key] || null);
            }
        }
    }])


})();