(function() {
    "use strict";

    angular
        .module('gift_app.controllers')
        .controller('challengeCtrl', challengeCtrl);


    /**
     * Controller for signup page - scoped to vm
     *
     * @param $state
     * @param $ionicLoading
     * @param $ionicPopup
     * @param cordovaPlugins
     */
    function challengeCtrl($state, $ionicPopover, $scope, Utils, $stateParams, $cordovaDeviceMotion, MyHttpRequest, $ionicLoading, $localstorage, $ionicPopup, cordovaPlugins, $ionicHistory) {
        var vm = this;
        var counter = 1;
        $scope.two_shakes = 'img/popup.png';
        $scope.goBack = function() {
            $ionicHistory.goBack();
        }

        // console.log($stateParams.obj);
        if ($stateParams.obj == null) {
            $scope.notification_data = $localstorage.getObject("notification");
        } else {
            $scope.notification_data = $stateParams.obj;
            // console.log("$scope.notification_data", $scope.notification_data);
        }

        $scope.showPopup = function() {
            $ionicPopover.fromTemplateUrl('templates/event/shake_popover.html', {
                scope: $scope
            }).then(function(popover) {
                $scope.popover = popover;
                $scope.startWatching();
                $scope.popover.show();
            });
        }
        $scope.close_popup = function() {
            $scope.popover.remove();
        }

        $scope.accept_challenge = function() {
            if (Utils.checkConnection()) {
                // $ionicLoading.show({
                //     template: '<p>Please wait...</p><ion-spinner icon="bubbles"></ion-spinner>'
                // })
                var params = {
                    bet_id: $scope.notification_data.bet_id,
                    status: 1
                }
                MyHttpRequest('bet/request', 'POST', params, false, true, true).then(function(result) {
                    $ionicLoading.hide();
                    // Utils.showToast("you have successfully accept challenge...", "short");
                    // console.log("Accept challenge Successfully... ", result);
                });
            } else {
                Utils.showToast("Please check your internet connection and try again.", "short");
            }
        }
        $scope.reject_challenge = function() {
            if (Utils.checkConnection()) {
                $ionicLoading.show({
                    template: '<p>Please wait...</p><ion-spinner icon="bubbles"></ion-spinner>'
                })
                var params = {
                    bet_id: $scope.notification_data.bet_id,
                    status: 2
                }
                MyHttpRequest('bet/request', 'POST', params, false, true, true).then(function(result) {
                    $ionicLoading.hide();
                    Utils.showToast("you have successfully reject challenge...", "short");
                    $state.go('home');
                });
            } else {
                Utils.showToast("Please check your internet connection and try again.", "short");
            }
        }

        // watch Acceleration options
        $scope.options = {
            frequency: 100, // Measure every 100ms
            deviation: 25 // We'll use deviation to determine the shake event, best values in the range between 25 and 30
        };
        // Current measurements
        $scope.measurements = {
                x: null,
                y: null,
                z: null,
                timestamp: null
            }
            // Previous measurements    
        $scope.previousMeasurements = {
            x: null,
            y: null,
            z: null,
            timestamp: null
        }

        //Start Watching method
        $scope.startWatching = function() {
            // Device motion configuration
            $scope.watch = $cordovaDeviceMotion.watchAcceleration($scope.options);
            // Device motion initilaization
            $scope.watch.then(null, function(error) {
                // console.log('Error');
            }, function(result) {
                // Set current data  
                $scope.measurements.x = result.x;
                $scope.measurements.y = result.y;
                $scope.measurements.z = result.z;
                $scope.measurements.timestamp = result.timestamp;
                // Detecta shake  
                $scope.detectShake(result);
            });
        };

        $scope.stopWatching = function() {
            $scope.watch.clearWatch();
        }

        // Detect shake method      
        $scope.detectShake = function(result) {
            //Object to hold measurement difference between current and old data
            var measurementsChange = {};
            // Calculate measurement change only if we have two sets of data, current and old
            if ($scope.previousMeasurements.x !== null) {
                measurementsChange.x = Math.abs($scope.previousMeasurements.x, result.x);
                measurementsChange.y = Math.abs($scope.previousMeasurements.y, result.y);
                measurementsChange.z = Math.abs($scope.previousMeasurements.z, result.z);
            }
            // If measurement change is bigger then predefined deviation
            if (measurementsChange.x + measurementsChange.y + measurementsChange.z > $scope.options.deviation) {
                if (counter == 1) {
                    counter++;
                    $scope.two_shakes = 'img/one.png';
                } else {
                    $scope.stopWatching(); // Stop watching because it will start triggering like hell
                    // console.log('Shake detected'); // shake detected
                    $scope.accept_challenge();
                    Utils.showToast('challenge accepted', 'short');
                    $scope.close_popup();
                    $state.go('home');
                }
                // alert('Shake detected');
                //setTimeout($scope.startWatching(), 1000); // Again start watching after 1 sec
                // Clean previous measurements after succesfull shake detection, so we can do it next time
                $scope.previousMeasurements = {
                    x: null,
                    y: null,
                    z: null
                }
            } else {
                // On first measurements set it as the previous one
                $scope.previousMeasurements = {
                    x: result.x,
                    y: result.y,
                    z: result.z
                }
            }
        }

        // $scope.startWatching();
        $scope.$on('$ionicView.beforeLeave', function() {
            $scope.stopWatching(); // Turn off motion detection watcher
        });

        // $scope.$on('$ionicView.enter', function(event, viewData) {
        //     $ionicHistory.clearCache();         
        // });

    }


})();