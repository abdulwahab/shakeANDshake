(function() {
    "use strict";

    angular
        .module('gift_app.controllers')
        .controller('createEventCtrl', createEventCtrl);


    /**
     * Controller for create Event page - scoped to vm
     *
     * @param $state
     * @param $ionicLoading
     * @param $ionicPopup
     * @param cordovaPlugins
     */
    function createEventCtrl($state, $ionicPopover, $scope, Utils, User_factory, $cordovaKeyboard, ApiEndpoint, $localstorage, MyHttpRequest, $ionicLoading, $ionicPopup, cordovaPlugins, $ionicHistory, $cordovaDeviceMotion) {
        var vm = this;

        $scope.current_btn;
        $scope.pri_type = "";
        $scope.select = true;
        $scope.selected_receiver = -1;
        $scope.obj = {
            "meBet": "",
            "frndBet": "",
            "betDate": new Date()
        }
        $scope.options = {
            frequency: 100, // Measure every 100ms
            deviation: 25 // We'll use deviation to determine the shake event, best values in the range between 25 and 30
        };
        var counter = 1;
        $scope.two_shakes = 'img/popup.png';
        // Current measurements
        $scope.measurements = {
                x: null,
                y: null,
                z: null,
                timestamp: null
            }
            // Previous measurements    
        $scope.previousMeasurements = {
            x: null,
            y: null,
            z: null,
            timestamp: null
        }

        var date = new Date();

        $scope.privacy_type = function(value) {
            Utils.hideKeyBoard();
            $scope.current_btn = value;
            if ($scope.current_btn == 1) {
                $scope.pri_type = "Invite Only";
            } else if ($scope.current_btn == 2) {
                $scope.pri_type = "All Friends";
            } else {
                $scope.pri_type = "Public";
            }
            $cordovaKeyboard.close();
        }

        $scope.select_frnd = function(reciver) {
            $scope.selected_receiver = reciver.id;
        }

        $scope.user = {
            "search": ""
        }

        $scope.$on('$ionicView.enter', function(event, viewData) {
            $scope.search_user();
        });


        $scope.textChanged = function() {
            if ($scope.user.search.length >= 3)
                $scope.search_user();
        };

        $scope.search_user = function() {
            if (Utils.checkConnection()) {
                $ionicLoading.show({
                    template: '<p>Please wait...</p><ion-spinner icon="bubbles"></ion-spinner>'
                })
                $scope._search = "";
                var params = {
                    key: $scope.user.search
                }
                MyHttpRequest('search/user', 'GET', params, false, true, true).then(function(result) {
                    $ionicLoading.hide();
                    if (result.status == 1) {
                        $scope._search = result;
                    }
                });
            } else {
                Utils.showToast("Please check your internet connection and try again.", "short");
            }
        }


        $scope.event_create = function() {
            if (Utils.checkConnection()) {
                $ionicLoading.show({
                    template: '<p>Please wait...</p><ion-spinner icon="bubbles"></ion-spinner>'
                })
                if ($scope.selected_receiver != -1) {
                    if ($scope.obj.meBet != '') {
                        if ($scope.obj.frndBet != '') {
                            if ($scope.obj.betDate != '' && $scope.obj.betDate >= date) {
                                if ($scope.current_btn != '') {
                                    var params = {
                                        receiver_id: $scope.selected_receiver,
                                        sender_bet: $scope.obj.meBet,
                                        receiver_bet: $scope.obj.frndBet,
                                        end_date: moment($scope.obj.betDate).format("YYYY-MM-DD"),
                                        privacy_type: $scope.pri_type
                                    }
                                    MyHttpRequest('add/bet', 'POST', params, false, true, true).then(function(result) {
                                        $ionicLoading.hide();
                                        Utils.showToast("Bet created successfully.", "short");
                                        window.history.back();
                                    });

                                } else {
                                    counter = 1;
                                    $scope.two_shakes = 'img/popup.png';
                                    $ionicLoading.hide();
                                    Utils.showToast("Please select privacy type ", "short");
                                }
                            } else {
                                counter = 1;
                                $scope.two_shakes = 'img/popup.png';
                                $ionicLoading.hide();
                                Utils.showToast("Please Enter valid Bet will End Date ", "short");
                            }
                        } else {
                            counter = 1;
                            $scope.two_shakes = 'img/popup.png';
                            $ionicLoading.hide();
                            Utils.showToast("Please Enter Friend Bet", "short");
                        }
                    } else {
                        $ionicLoading.hide();
                        counter = 1;
                        $scope.two_shakes = 'img/popup.png';
                        Utils.showToast("Please Enter Me Bet", "short");
                    }
                } else {
                    counter = 1;
                    $scope.two_shakes = 'img/popup.png';
                    $ionicLoading.hide();
                    Utils.showToast("Please select Friend first", "short");
                }
            } else {
                Utils.showToast("Please check your internet connection and try again.", "short");
            }
        }

        $scope.goBack = function() {
            $ionicHistory.goBack();
        }

        $scope.showPopup = function() {
            $ionicPopover.fromTemplateUrl('templates/event/shake_popover.html', {
                scope: $scope
            }).then(function(popover) {
                $scope.popover = popover;
                $scope.startWatching();
                $scope.popover.show();
            });
        }

        //shake detection
        $scope.startWatching = function() {
            // Device motion configuration
            $scope.watch = $cordovaDeviceMotion.watchAcceleration($scope.options);
            // Device motion initilaization
            $scope.watch.then(null, function(error) {
                // console.log('Error');
            }, function(result) {
                // Set current data  
                $scope.measurements.x = result.x;
                $scope.measurements.y = result.y;
                $scope.measurements.z = result.z;
                $scope.measurements.timestamp = result.timestamp;
                // Detecta shake  
                $scope.detectShake(result);
            });
        };

        $scope.close_popup = function() {
            $scope.popover.remove();
            $scope.stopWatching();
        }

        $scope.stopWatching = function() {
            if ($scope.watch)
                $scope.watch.clearWatch();
        }

        // Detect shake method      
        $scope.detectShake = function(result) {
            //Object to hold measurement difference between current and old data
            var measurementsChange = {};
            // Calculate measurement change only if we have two sets of data, current and old
            if ($scope.previousMeasurements.x !== null) {
                measurementsChange.x = Math.abs($scope.previousMeasurements.x, result.x);
                measurementsChange.y = Math.abs($scope.previousMeasurements.y, result.y);
                measurementsChange.z = Math.abs($scope.previousMeasurements.z, result.z);
            }
            // If measurement change is bigger then predefined deviation
            if (measurementsChange.x + measurementsChange.y + measurementsChange.z > $scope.options.deviation) {
                if (counter == 1) {
                    counter++;
                    $scope.two_shakes = 'img/one.png';
                } else {
                    $scope.stopWatching();
                    $scope.event_create();
                    $scope.popover.remove();
                    // setTimeout($scope.startWatching(), 1000);
                }
                $scope.previousMeasurements = {
                    x: null,
                    y: null,
                    z: null
                }
            } else {
                // On first measurements set it as the previous one
                $scope.previousMeasurements = {
                    x: result.x,
                    y: result.y,
                    z: result.z
                }
            }
        }

        // $scope.startWatching();
        $scope.$on('$ionicView.beforeLeave', function() {
            $scope.stopWatching(); // Turn off motion detection watcher
        });

    }


})();