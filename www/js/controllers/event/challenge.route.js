(function() {
    "use strict";

    angular
        .module('gift_app.controllers')
        .config(challengeRoute);


    /**
     * Route declaration of the signup states
     *
     * @param $stateProvider
     */
    function challengeRoute($stateProvider) {
        $stateProvider

            .state('challenge', {
            url: '/challenge',
            templateUrl: 'templates/event/challenge.html',
            controller: 'challengeCtrl as vm',
            params: {
                obj: null
            }
        })
    }

})();
