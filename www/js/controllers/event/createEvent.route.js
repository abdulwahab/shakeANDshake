



(function() {
    "use strict";

    angular
        .module('gift_app.controllers')
        .config(createEventRoute);


    /**
     * Route declaration of the signup states
     *
     * @param $stateProvider
     */
    function createEventRoute($stateProvider) {
        $stateProvider

            .state('createEvent', {
            url: '/createEvent',
            templateUrl: 'templates/event/create_event.html',
            controller: 'createEventCtrl as vm',
            data: {
                secure: false
            }
        })
    }

})();