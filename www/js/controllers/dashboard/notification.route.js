



(function() {
    "use strict";

    angular
        .module('gift_app.controllers')
        .config(notificationRoute);


    /**
     * Route declaration of the signup states
     *
     * @param $stateProvider
     */
    function notificationRoute($stateProvider) {
        $stateProvider

            .state('notification', {
            url: '/notification',
            templateUrl: 'templates/dashboard/notification.html',
            controller: 'notificationsCtrl as vm',
            data: {
                secure: false
            }
        })
    }

})();