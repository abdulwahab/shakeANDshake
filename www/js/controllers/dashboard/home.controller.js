(function() {
    "use strict";

    angular
        .module('gift_app.controllers')
        .controller('homeCtrl', homeCtrl);



    function homeCtrl($scope, $rootScope, Utils, $state, ApiEndpoint, $ionicTabsDelegate, $ionicPopover, MyHttpRequest, $localstorage, $cordovaFacebook, $cordovaDeviceMotion, $cordovaSocialSharing, $ionicPopup, $ionicLoading, cordovaPlugins, $ionicHistory) {
        var vm = this;

        $scope.events = {
            filter: 'activity',
            tab: 'my',
            public_page_no: 0,
            activity_page_no: 0,
            friends_page_no: 0
        }

        $scope.current_btn = 1;
        $scope.current_btn_name = 'all';
        $scope.pageNumber = 1;
        $scope.moredata = true;
        $scope.bet_data = [];
        $scope.no_bet_check = false;
        $scope.app_invite_btn = false;

        var user_data = $localstorage.getObject("login_userData");

        $scope.profileImge = user_data.data.picture;

        $scope.showPopup = function() {
            $ionicPopover.fromTemplateUrl('templates/dashboard/log_out.html', {
                scope: $scope
            }).then(function(popover) {
                $scope.popover = popover;
                $scope.popover.show();
            });
        }
        $scope.close_popup = function() {
            $scope.popover.remove();
        }

        $scope.$on('session_expired', function() {
            Utils.showToast("Session expired.", "short");
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true,
                historyRoot: true
            });
            $state.go('signin');
        })

        $scope.logOut = function() {
            if (Utils.checkConnection()) {
                $ionicLoading.show({
                    template: '<p></p><ion-spinner icon="bubbles"></ion-spinner>'
                })
                var params = {}
                MyHttpRequest('logout', 'POST', params, false, true, true).then(function(result) {
                    $ionicLoading.hide();
                    $scope.popover.remove();
                    localStorage.removeItem('login_userData');
                    localStorage.removeItem('_token');
                    localStorage.removeItem('notification');
                    $ionicHistory.clearHistory();
                    $ionicHistory.clearCache();
                    $ionicHistory.nextViewOptions({
                        disableAnimate: true,
                        disableBack: true,
                        historyRoot: true
                    });
                    $state.go('signin');

                });
            } else {
                Utils.showToast("Please check your internet connection and try again.", "short");
            }
        }

        $scope.sharefb = function(item) {
            // console.log(item);
            var message = item.receiver_bet + " Vs " + item.sender_bet;
            // console.log("message", message);
            $cordovaSocialSharing
                .shareViaFacebook(message, null, null)
                .then(function(result) {
                    // Success!
                    // console.log("Success", result);
                }, function(err) {
                    // An error occurred. Show a message to the user
                    // console.log("error", err);
                });
        }

        $scope.current_tab = function(value) {
            if ($scope.no_bet_check)
                $scope.no_bet_check = false;

            if (value == 1 && $scope.current_btn != value) {
                $scope.current_btn = value;
                $scope.current_btn_name = 'all';
                $scope.moredata = true;
                $scope.pageNumber = 1;
                $scope.bet_data = [];
                if ($scope.events.filter == 'friends') {
                    $scope.app_invite_btn = false;
                }
                $scope.loadMore();
            } else if (value == 2 && $scope.current_btn != value) {
                $scope.current_btn = value;
                $scope.current_btn_name = 'pending';
                $scope.moredata = true;
                $scope.pageNumber = 1;
                $scope.bet_data = [];
                $scope.loadMore();
            } else if (value == 3 && $scope.current_btn != value) {
                $scope.current_btn = value;
                $scope.current_btn_name = 'completed';
                $scope.moredata = true;
                $scope.pageNumber = 1;
                $scope.bet_data = [];
                $scope.loadMore();
            }
            //$scope.filter($scope.events.filter);
        }

        $scope.swipeLeft = function() {
            if ($scope.current_btn == 3)
                $scope.current_btn_name = 2;
            else if ($scope.current_btn == 2)
                $scope.current_btn_name = 1;
            else
                $scope.current_btn_name = 3;
            $scope.current_tab($scope.current_btn_name);
        }
        $scope.swipeRight = function() {
            if ($scope.current_btn == 1)
                $scope.current_btn_name = 2;
            else if ($scope.current_btn == 2)
                $scope.current_btn_name = 3;
            else
                $scope.current_btn_name = 1
            $scope.current_tab($scope.current_btn_name);
        }

        $scope.$on('$ionicView.enter', function(event, viewData) {
            if (event.targetScope !== $scope)
                return;
        });

        $scope.filter = function(type) {
            if ($scope.type != $scope.events.filter) {
                $scope.events.activity_page_no = 0;
                $scope.events.friends_page_no = 0;
                $scope.events.public_page_no = 0;
            }
            if ($scope.events.filter == 'friends') {
                $scope.app_invite_btn = false;
            }
            $scope.no_bet_check = false;
            $scope.events.filter = type;
            $scope.moredata = true;
            $scope.pageNumber = 1;
            $scope.bet_data = [];
        }

        $scope.moreDataCanBeLoaded = function() {
            return $scope.moredata;
        }

        $scope.loadMore = function() {
            if (Utils.checkConnection()) {
                var url;
                if ($scope.events.filter == 'activity') {
                    url = 'get/my_bets';
                } else if ($scope.events.filter == 'public') {
                    url = 'get/public_bets';
                } else {
                    url = 'get/friends_bets';
                }

                var params = {
                    filter_param: $scope.current_btn_name,
                    number: $scope.pageNumber
                }
                MyHttpRequest(url, 'GET', params, false, true, true).then(function(result) {
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    if (result.status == 1) {
                        if (result.data.bets.length > 0)
                            $scope.bet_data = result.data.bets;
                        else
                            $scope.no_bet_check = true;
                        if (result.data.page == null)
                            $scope.moredata = false;
                        else
                            $scope.moredata = true;

                        $scope.pageNumber = result.data.page;
                        if ($scope.events.filter == 'friends') {
                            $scope.app_invite_btn = true;
                        }
                    } else if (result == null || result.code == 401) {
                        $state.go("signin");
                        localStorage.removeItem('login_userData');
                        localStorage.removeItem('_token');
                        localStorage.removeItem('notification');
                    } else
                        Utils.showToast("Couldn't load bets", "short");
                });
            } else {
                Utils.showToast("Please check your internet connection and try again.", "short");
            }
        }

        $scope.invite = function() {
            facebookConnectPlugin.appInvite({
                    url: "https://fb.me/829951207156835",
                    picture: "https://fb-s-a-a.akamaihd.net/h-ak-xtl1/v/t1.0-1/c0.0.99.99/16729018_101937566997047_4406293573702493587_n.jpg?oh=cf7e79a8c508d529cbe3995c4454ae45&oe=5987CD98&__gda__=1501392788_449e03976d7c8f4717defe4fd76579b9",
                    "access_token": "EAALlcYSW0GIBAAQXbmJPoBAm7dCEuxlVJ7kjjZCcjOKOiIIHcY9z1QZAu6qZCBqCfM2NUVxZBzWpC56XfFYyHg0oyIRDuOIdFaJBCi2ZBsvFjphO3eOFqApKGV09bNZBrFCNWZCRz1FBm5PU9CjSSDS3qem3pP8U529O6yOTdYEuCHAezf9D39fTX7ilaDsNfKrPbYX6084GHSseQioK0Kq"
                },
                function(obj) {
                    if (obj) {
                        if (obj.completionGesture == "cancel") {
                            // user canceled, bad guy
                        } else {
                            // user really invited someone :)
                        }
                    } else {
                        // user just pressed done, bad guy
                    }
                },
                function(obj) {
                    // error
                    // console.log(obj);
                }
            );
        }
    }

})();