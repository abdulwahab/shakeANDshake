
(function() {
    "use strict";

    angular
        .module('gift_app.controllers')
        .config(homeRoute);


    /**
     * Route declaration of the signin state
     *
     * @param $stateProvider
     */
    function homeRoute($stateProvider) {
        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'templates/dashboard/home.html',
                controller: 'homeCtrl as vm',
                data: {
                    secure: false
                }
            });
    }

})();
