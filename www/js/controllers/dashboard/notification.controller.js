(function() {
    "use strict";

    angular
        .module('gift_app.controllers')
        .controller('notificationsCtrl', notificationsCtrl);


    /**
     * Controller for signup page - scoped to vm
     *
     * @param $state
     * @param $ionicLoading
     * @param $ionicPopup
     * @param cordovaPlugins
     */
    function notificationsCtrl($state, $ionicPopover, $scope, Utils, MyHttpRequest, $localstorage, $ionicLoading, $ionicPopup, cordovaPlugins, $ionicHistory) {
        var vm = this;

        $scope.goBack = function() {
            $ionicHistory.goBack();
        }

        $scope.pageNumber = 1;
        $scope.moredata = true;
        $scope.notify_data = [];
        $scope.no_notifications = false;

        $scope.moreDataCanBeLoaded = function() {
            return $scope.moredata;
        }

        $scope.loadMore = function() {
            if (Utils.checkConnection()) {
                var params = {
                    number: $scope.pageNumber
                }
                MyHttpRequest('get/notifications', 'GET', params, false, true, true).then(function(result) {
                    if (result.status == 1) {
                        $localstorage.setObject("notification", $scope.notify_data);
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                        $scope.notify_data = $scope.notify_data.concat(result.data.notifications);
                        if (result.data.page == null)
                            $scope.moredata = false;
                        else
                            $scope.moredata = true;
                        $scope.pageNumber = result.data.page;
                        if ($scope.notify_data.length == 0) {
                            $scope.no_notifications = true;
                        }
                    } else {
                        Utils.showToast("Couldn't load notification", "short");
                    }

                });
            } else {
                Utils.showToast("Please check your internet connection and try again.", "short");
            }
        }

        $scope.challenge_screen = function(obj) {
            $state.go("challenge", { obj: obj });
        }

    }


})();