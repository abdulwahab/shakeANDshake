(function() {
    "use strict";

    angular
        .module('gift_app.controllers')
        .controller('SignInCtrl', SignInCtrl);


    /**
     * Controller for signin page - scoped to vm
     *
     * @param $scope
     * @param $rootScope
     * @param $state
     * @param $ionicLoading
     * @param cordovaPlugins
     */
    function SignInCtrl($scope, $rootScope, $state, ApiEndpoint, User_factory, $localstorage, MyHttpRequest, $cordovaFacebook, $ionicPopup, $ionicLoading, cordovaPlugins, $ionicHistory, Utils) {
        var vm = this;

        document.addEventListener("offline", onOffline, false);

        function onOffline() {
            Utils.showToast("please turn on Wifi/Mobile data", "short");

        }


        angular.extend(vm, {
            user: {
                email_address: '',
                password: ''
            }
        });

        function facebook_data() {
            $cordovaFacebook.api("me?fields=id,email,first_name,last_name,gender,picture", ["email", "public_profile"])
                .then(function(fb_result) {
                    // var params = {
                    //     access_token: ,
                    //     device_type: ,
                    //     device_token: 
                    // }
                    // MyHttpRequest('fblogin', 'POST', params, false, true, true).then(function(result) {
                    //     $ionicLoading.hide();
                    //     if(result != null){
                    //     Utils.showToast("Successfully login", 'short');    
                    // }else{
                    //     Utils.showToast("something went wrong", 'short');
                    // }

                    // });

                    $ionicLoading.hide();


                    $scope.profileImge = "http://graph.facebook.com/" + fb_result.id + "/picture?type=square";
                    $scope.user_name = fb_result.first_name;

                    // $scope.invite($scope.profileImge);

                    console.log(fb_result, " result of fb login");

                }, function(error) {
                    $ionicLoading.hide();
                });
        }

        $scope.fbLogin = function() {
            if (Utils.checkConnection()) {
                $ionicLoading.show({
                    template: '<ion-spinner icon="bubbles"></ion-spinner>'
                })
                var currentPlatform = ionic.Platform.platform();
                var device_id = User_factory.get_user();
                // console.log("device_id", device_id, currentPlatform, "currentPlatform");
                $cordovaFacebook.getLoginStatus()
                    .then(function(result) {
                        // console.log("result data is ...", result);
                        $cordovaFacebook.login(["public_profile", "email", "user_friends"])
                            .then(function(success) {
                                var params = {
                                    access_token: success.authResponse.accessToken,
                                    device_type: currentPlatform,
                                    device_token: device_id
                                }
                                MyHttpRequest('fblogin', 'POST', params, false, true, true).then(function(result) {
                                    $ionicLoading.hide();
                                    if (result != null) {
                                        $localstorage.set('_token', result.data.access_token);
                                        $localstorage.setObject("login_userData", result);
                                        // console.log("api hit response is ..... in if", result);
                                        // Utils.showToast("Successfully login", 'short');
                                        $state.go("home");
                                    } else {
                                        Utils.showToast("Couldn't login", 'short');
                                    }

                                });
                            }, function(error) {
                                $ionicLoading.hide();
                            });
                    }, function(error) {
                        // console.log("error is receive .. ", error);
                        Utils.showToast("Could not logg in", "long");
                    });
            } else {
                Utils.showToast("Please check your internet connection and try again.", "short");
            }
        }

        $scope.$on('$ionicView.enter', function(event, viewData) {
            $ionicHistory.clearCache();
        });

    }
})();