
(function() {
    "use strict";

    angular
        .module('gift_app.controllers')
        .config(signInRoute);


    /**
     * Route declaration of the signin state
     *
     * @param $stateProvider
     */
    function signInRoute($stateProvider) {
        $stateProvider
            .state('signin', {
                url: '/signin',
                templateUrl: 'templates/auth/signin.html',
                controller: 'SignInCtrl as vm',
                data: {
                    secure: false
                }
            });
    }

})();
