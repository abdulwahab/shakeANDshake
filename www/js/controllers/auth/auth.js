/*
 * @Author: bilalakmal
 * @Date:   2017-02-14 16:57:52
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-14 16:59:12
 */

(function() {
    "use strict";

    angular.module('gift_app.controllers.auth', []);

})();
