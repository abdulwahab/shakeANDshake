/*
 * @Author: bilalakmal
 * @Date:   2017-02-14 16:53:45
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-14 16:54:54
 */

(function() {
    "use strict";

    angular.module('gift_app.controllers', []);

})();
