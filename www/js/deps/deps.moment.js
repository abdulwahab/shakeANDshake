/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:31:36
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:31:42
 */


(function() {
    "use strict";

    angular
        .module('gift_app.deps')
        .factory('moment', ['$window', momentFactory]);


    /**
     * Factory implementation of wrapper for underscore - used to inject into angular
     *
     * @param $window
     * @returns {*}
     */
    function momentFactory($window) {
        return $window.moment;
    }

})();
