/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:32:37
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:32:42
 */


(function() {
    "use strict";

    angular
        .module('gift_app.deps')
        .factory('_', ['$window', underscoreFactory]);


    /**
     * Factory implementation of wrapper for underscore - used to inject into angular
     *
     * @param $window
     * @returns {*}
     */
    function underscoreFactory($window) {
        return $window._;
    }

})();
