/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:30:04
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:30:19
 */

(function() {
    "use strict";

    angular
        .module('gift_app.deps')
        .factory('cordovaPlugins', ['_', '$q', '$cordovaToast', '$cordovaVibration', '$cordovaCamera', '$cordovaFileTransfer', '$cordovaInAppBrowser', '$cordovaContacts', cordovaPluginsFactory]);


    /**
     * Factory implementation of wrapper over cordova plugins
     *
     * @param $q
     * @param $cordovaToast
     * @param $cordovaVibration
     * @returns {{doToast: doToast, doVibrate: doVibrate}}
     */
    function cordovaPluginsFactory(_, $q, $cordovaToast, $cordovaVibration, $cordovaCamera, $cordovaFileTransfer, $cordovaInAppBrowser, $cordovaContacts) {
        return {
            doToast: doToast,
            doVibrate: doVibrate,
            choosePhoto: choosePhoto,
            capturePhoto: capturePhoto,
            upload_file: upload_file,
            download_file: download_file,
            open_site_via_inappBrowser: open_site_via_inappBrowser,
            choosePhoto_FileURI: choosePhoto_FileURI,
            getContactsWithEmail: getContactsWithEmail
        }

        //////////

        /**
         * Follows the rules of a promise - checks if the platform is a device or not due to incompatibilities of cordova plugins within browsers
         *
         * @param doJob
         * @returns {*}
         */
        function isDevice(doJob) {
            return $q(function(resolve, reject) {
                // ionic.Platform.isWebView() sometimes does not work
                // undefined in webView
                if (!!ionic.Platform.device().uuid)
                    resolve(doJob.call()); // continue processing

                reject('ngCordova plugins not supported in browser'); // throws an error
            });
        }

        function open_site_via_inappBrowser(url) {
            if (!$cordovaInAppBrowser)
                return;
            var options = {
                location: 'yes',
                clearcache: 'yes',
                toolbar: 'no'
            };

            $cordovaInAppBrowser.open(url, '_system', options)
                .then(function(event) {})
                .catch(function(event) {});
        }

        /**
         * Wrapper for cordova toast - displays a device notification alert within the webview
         *
         * @param settings
         * @returns {*}
         */
        function doToast(settings) {
            return isDevice(function() {
                // define default values
                var def = _.defaults(settings, { duration: 'long', position: 'bottom' });
                return $cordovaToast.show(def.message, def.duration, def.position);
            });
        }

        /**
         * Wrapper for cordova vibrate
         *
         * @param settings
         * @returns {*}
         */
        function doVibrate(settings) {
            return isDevice(function() {
                // define default values
                var def = { duration: 100 };
                return $cordovaVibration.vibrate(def.duration);
            });
        }

        /**
         * Open photo gallery
         *
         * @returns {*}
         */
        function choosePhoto() {
            return isDevice(function() {
                var options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    popoverOptions: CameraPopoverOptions
                };

                var q = $q.defer();

                $cordovaCamera.getPicture(options).then(function(imageData) {
                    q.resolve(imageData);
                }, function(error) {
                    q.reject(error);
                });

                return q.promise;
            });
        }

        /**
         * Open photo gallery with File URI option
         *
         * @returns {*}
         */
        function choosePhoto_FileURI() {
            return isDevice(function() {
                var options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    popoverOptions: CameraPopoverOptions
                };

                var q = $q.defer();

                $cordovaCamera.getPicture(options).then(function(imageData) {
                    q.resolve(imageData);
                }, function(error) {
                    q.reject(error);
                });

                return q.promise;
            });
        }

        /**
         * File Transfer Upload
         *
         * @returns {*}
         */
        function upload_file(options, url, file_path) {
            return isDevice(function() {
                console.log(file_path);
                var trustHosts = true;

                var q = $q.defer();

                $cordovaFileTransfer.upload(url, file_path, options)
                    .then(function(result) {
                        q.resolve(result);
                        // Success!
                    }, function(err) {
                        q.reject(err);
                        // Error
                    }, function(progress) {
                        // console.log(progress.loaded / progress.total * 100);
                    });

                return q.promise;
            });
        }

        /**
         * File Transfer Download
         *
         * @returns {*}
         */
        function download_file(options, url, file_path) {
            return isDevice(function() {
                console.log(file_path);
                var trustHosts = true;

                var q = $q.defer();

                $cordovaFileTransfer.download(url, file_path, options, trustHosts)
                    .then(function(result) {
                        q.resolve(result);
                        // Success!
                    }, function(err) {
                        q.reject(err);
                        // Error
                    }, function(progress) {
                        // console.log(progress.loaded / progress.total * 100);
                    });

                return q.promise;
            });
        }

        /**
         * Open photo gallery
         *
         * @returns {*}
         */
        function capturePhoto() {
            return isDevice(function() {
                var options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true,
                    correctOrientation: true
                };

                var q = $q.defer();

                $cordovaCamera.getPicture(options).then(function(imageData) {
                    q.resolve(imageData);
                }, function(error) {
                    q.reject(error);
                });

                return q.promise;
            });
        }

        /**
         * Get Contacts from user's phone
         *
         * @returns {*}
         */
        function getContactsWithEmail() {
            return isDevice(function() {
                var q = $q.defer();

                $cordovaContacts.find({}).then(function(contacts) {
                    var matches = [];

                    angular.forEach(contacts, function(value, key) {
                        if (value.emails != null) {
                            matches.push(value);
                        }
                    });

                    q.resolve(matches);

                });

                return q.promise;
            });
        }

    }

})();
