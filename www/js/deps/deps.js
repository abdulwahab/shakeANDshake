/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:31:16
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:31:22
 */

(function() {
    "use strict";

    angular.module('gift_app.deps', []);

})();
