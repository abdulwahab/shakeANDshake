/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 18:40:51
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-14 16:49:02
 */


(function() {
    "use strict";

    angular
        .module('gift_app.deps')

    .filter('secondsToDateTime', function() {
        return function(seconds) {
            var d = new Date(0, 0, 0, 0, 0, 0, 0);
            d.setSeconds(seconds);
            return d;
        };
    })

    .filter('daysAndHoursDifference', function() {
        return function(timestamp) {
            var date = new Date(timestamp);
            date = moment(date);
            var current_date = moment(new Date());
            var days_diff = date.diff(current_date, 'days');
            var hours_diff = date.diff(current_date, 'hours');
            hours_diff = hours_diff % 24;
            if (hours_diff > 0)
                return days_diff + ' days, ' + hours_diff + ' hours remaining';
            else
                return days_diff + ' days';
        }
    })

    .filter('ageFilter', function() {
        function calculateAge(birthday) { // birthday is a date
            birthday = new Date(birthday);
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }

        return function(birthdate) {
            return calculateAge(birthdate);
        };
    })

    .filter('notification_relative_time', ['$sce', function($sce) {
        return function(timestamp) {
            var date = new Date(new Date(timestamp * 1000).getTime() + (new Date().getTimezoneOffset() * 60 * 1000));
            return moment(date).fromNow();
        };
    }])

    .filter('last_check_in_relative_time', ['$sce', function($sce) {
        return function(date) {
            date = new Date(date);
            date = new Date(date.getTime() - (new Date().getTimezoneOffset() * 60 * 1000));
            return moment(date).fromNow();
        };
    }])

    .filter('trusted', ['$sce', function($sce) {
        return function(url) {
            return $sce.trustAsResourceUrl(url);
        };
    }])

    .filter('rawHtml', ['$sce', function($sce) {
        return function(val) {
            return $sce.trustAsHtml(val);
        }
    }]);

})();
