/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:29:03
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:29:10
 */

(function() {
    "use strict";

    angular
        .module('gift_app.components')
        .component('progressBar', {
            transclude: true,
            bindings: {
                value: '<'
            },
            templateUrl: 'js/components/bars/progress-bar.html'
        })

    ;

})();
