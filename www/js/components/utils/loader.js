/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 18:57:25
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:29:21
 */

(function() {
    "use strict";

    angular
        .module('gift_app.components')

    .component('contentLoader', {
        templateUrl: 'js/components/utils/loader.html'
    });

})();
