/*
 * @Author: bilalakmal
 * @Date:   2017-02-13 19:29:31
 * @Last Modified by:   bilalakmal
 * @Last Modified time: 2017-02-13 19:29:37
 */


(function() {
    "use strict";

    angular.module('gift_app.components', []);

})();
